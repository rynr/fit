# renovate

This project runs [renovate-bot][renovate] for my [projects][rynr].

[renovate]: https://renovatebot.com/
[rynr]: https://gitlab.com/rynr/
